---
title: "Český rozhlas představuje kandidáty do Senátu"
perex: "Šest otázek a stejný časový limit. Český rozhlas představuje všechny kandidáty do Senátu, kteří se rozhodli využít nabídku na natočení takzvaných předvolebních vizitek. Můžete si tak na jednom místě snadno porovnat, kdo usiluje o vaše hlasy. O senátorská křesla se letos utká celkem 233 lidí. To je o šest více než v předchozích volbách před šesti lety. První kolo voleb proběhne 7. a 8. října, druhé o týden později, tedy 14. a 15. října 2016."
description: "Šest otázek a stejný časový limit. Český rozhlas představuje všechny kandidáty do Senátu, kteří se rozhodli využít nabídku na natočení takzvaných předvolebních vizitek. Můžete si tak na jednom místě snadno porovnat, kdo usiluje o vaše hlasy. O senátorská křesla se letos utká celkem 233 lidí. To je o šest více než v předchozích volbách ve stejných obvodech před šesti lety."
authors: ["Anna Kottová", "Regionální stanice ČRo", "Marcel Šulek"]
published: "5. září 2016"
socialimg: https://interaktivni.rozhlas.cz/data/volby-2016-vizitky/www/media/mapa.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "kandidati-do-senatu"
libraries: [d3, topojson]
recommended:
  - link: https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/
    title: Český rozhlas představuje kandidáty do krajských zastupitelstev
    perex: Kdo bude vaším příštím hejtmanem? Regionální stanice Českého rozhlasu nabídly všem jedničkám na kandidátkách stejný prostor a možnost odpovědět na stejné otázy; celkem oslovily 269 kandidátů. Poslechněte si jejich odpovědi a rozhodněte se, komu dáte svůj hlas ve volbách 7. a 8. října.
    image: https://interaktivni.rozhlas.cz/data/volby-2016-vizitky-kraje/www/media/mapa.png
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?
    image: https://interaktivni.rozhlas.cz/data/kandidatky-2016-text/www/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/yusra/
    title: Yusra plave o život: Cesta syrské uprchlice z Damašku do olympijského Ria
    perex: Uprchlická celebrita, olympijská plavkyně. Osmnáctiletá Yusra Mardini je nyní hvězdou v záři reflektorů. V srpnu 2015 však byla na útěku. Uprchla se svou sestrou Sarah hledat nový domov. Reportérka Magdalena Sodomková a fotograf Lam Duc Hien zachytili její putování přes Egejské moře do Evropy.
    image: https://interaktivni.rozhlas.cz/yusra/media/fb2.jpg
---

<div class="ig" data-ig="base"></div>

Stejný prostor [nabídl Český rozhlas také lídrům všech krajských kandidátek](https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/).

Podoba volebních vizitek je pro kandidáty jednotná. První otázka zní „Proč by lidé měli volit právě vás?“ a na odpověď mají všichni jednu minutu. Časový limit u dalších pěti otázek je 30 sekund. Kandidáti samozřejmě můžou odpovědět v kratším čase, ušetřené sekundy se ale nepřevádějí.

Český rozhlas se tedy dále ptal:

- Jaký nový zákon nebo novelu byste navrhoval jako první? 
- Jak byste chtěl/a zvýšit důvěryhodnost Senátu? 
- Co je nejpalčivějším problémem vašeho volebního obvodu a jak chcete přispět k jeho řešení? 
- Co byste konkrétně udělal pro zlepšení politické kultury v České republice, tedy nejen v Senátu? 
- Budete se v případě zvolení věnovat senátorské práci naplno, nebo se budete chtít souběžně věnovat i jiným zaměstnaneckým, podnikatelským, politickým nebo dalším aktivitám?

Kandidáti do Senátu sice všechny otázky předem znali, při natáčení ale museli odpovídat z hlavy a na první pokus. Nemohli tedy mít před sebou připravený text ani poznámky na papíře nebo v mobilním telefonu či tabletu. Někteří ale nabídku na natočení volební vizitky odmítli.

Rozhovory s jednotlivými kandidáty se ve vysílání Českého rozhlasu objeví postupně. Pořadí určil los, na který dohlížel notář.