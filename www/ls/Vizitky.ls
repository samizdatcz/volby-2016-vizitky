prefix = "https://samizdat.blob.core.windows.net/vizitky16"
class ig.Vizitky
  (@parentElement, @data) ->
    @element = @parentElement.append \div
      ..attr \class \vizitky
    @header = @element.append \h1
    @list = @element.append \ul

  display: (subset, heading, filter) ->

    location = if window.top?location
      that.href
    else
      window.location.href
    location = location.split '#' .0
    @header.html heading
    @list.html ""
    displayed = @data[subset].filter filter
    @list.selectAll \li .data displayed .enter!append \li
      ..append \div
        ..attr \class \right
        ..append \h2
          ..append \span
            ..attr \class \cislo
            ..html (.cislo)
          ..append \span
            ..html -> "#{it.jmeno} #{it.prijmeni}"
        ..append \span
          ..attr \class \strana
          ..html ->
            out = ""
            if it.prislusnost == "BEZPP"
              out += "Bezpartijní"
            else
              out += "Člen #{it.prislusnost}"
            out + ", navrhuje #{it.navrhujici_strana}"
        ..append \span
          ..attr \class \supplemental
          ..html -> "#{capitalize it.povolani}, #{it.vek} let"
        ..append \div
          ..html -> 
            if it.mp3
              "<audio src='#prefix/#{it.mp3}' preload='none' controls='yes'>"
            else
              "<span class='supplemental'>#{it.pozn}</span>"
        ..append \span
          ..attr \class \share
          ..html ->
            link = escape do
              location + '#' + "#{it.obvod}-#{it.cislo}"
            "Sdílet na <a href='https://www.facebook.com/sharer/sharer.php?u=#link'>Facebook</a> | <a href='https://twitter.com/home?status=#link'>Twitter</a>"
      ..append \div
        ..attr \class \left
        ..append \img
          ..attr {width: 120, height: 180}
          ..attr \alt -> "#{it.jmeno} #{it.prijmeni}"
          ..attr \src -> "#prefix/#{it.foto}"

capitalize = ->
  it[0].toUpperCase! + it.substr 1
