senatObvody = [1 4 7 10 13 16 19 22 25 28 31 34 37 40 43 46 49 52 55 58 61 64 67 70 73 76 79]
class ig.Mapka
  (@container, @data) ->
    @element = @container.append \div
      ..attr \class \mapka
    @svg = @element.append \svg
    @select = @element.append \select
    ig.Events @
    @currentObvod = null
    window.addEventListener \resize @~resize

  draw: (part) ->
    topo = @data[part]
    options = features = @features = topojson.feature topo, topo.objects."data" .features
    if part == \senat
      for feature in features
        feature.properties.isInactive = not (feature.properties.VOL_OKR in senatObvody)
      options = features.filter -> !it.properties.isInactive
    {width, height, projection} = ig.utils.geo.getFittingProjection features, {width: @element.node!offsetWidth - 30}
    @fullWidth = width
    @fullHeight = height
    path = d3.geo.path!
      ..projection projection
    @select.html ''
    @svg
      ..attr {width, height}
      ..html ""
    @paths = @svg.selectAll \path .data features .enter!append \path
      ..attr \d -> path it
    if part == \senat
      @paths
        ..attr \data-tooltip ->
          o = it.properties.VOL_OKR + " – " + it.properties.SIDLO
          if it.properties.isInactive
            o += " – v tomto obvodě se letos nevolí"
          o
        ..classed \is-inactive -> it.properties.isInactive
        ..on \click @~onObvodSelected
        ..on \touchstart @~onObvodSelected
      optionsAssoc = {}
      for option in options
        optionsAssoc[option.properties.VOL_OKR] = option
      @select.append \option
        ..html "Vyberte senátní obvod…"
        ..attr \value ""
      @select.selectAll \option.foo .data options .enter!append \option
        ..html -> it.properties.VOL_OKR + " – " + it.properties.SIDLO
        ..attr \value (.properties.VOL_OKR)
      @select.on \change ~>
        value = @select.node!value
        if value
          obvod = optionsAssoc[value]
          return if @currentObvod == obvod
          @onObvodSelected obvod


  resize: ->
    return unless @fullWidth
    ratio = (@element.node!offsetWidth - 30) / @fullWidth
    @svg.style \transform "scale(#ratio)"
    @element.style \height "#{@fullHeight * ratio}px"

  onObvodSelected: (obvod) ->
    if obvod.properties.isInactive
      return
    @currentObvod = obvod
    @emit \obvod obvod.properties.VOL_OKR, obvod.properties.SIDLO
    @select.node!value = obvod.properties.VOL_OKR
    @paths.classed \active -> it is obvod

