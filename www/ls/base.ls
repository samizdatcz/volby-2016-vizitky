new Tooltip!watchElements!
container = d3.select ig.containers.base
senat = d3.csv.parse ig.data.senat
senatAssoc = {}
for jouda in senat
  senatAssoc["#{jouda.obvod}-#{jouda.cislo}"] = jouda
mapka = new ig.Mapka container, {senat: ig.data.senatTopo}
  ..draw \senat
  ..on \obvod (obvod, nazev) ->
      container.classed \is-share 0
      invitation.remove!
      obvod .= toString!
      vizitky.display do
        \senat
        "Senátní obvod #{obvod} – #{nazev}"
        -> it.obvod == obvod

  
obvodyAssoc = {}
console.log mapka.features
for obvod in mapka.features
  obvodyAssoc[obvod.properties.VOL_OKR] = obvod

vizitky = new ig.Vizitky container, {senat}
# vizitky.display \senat "Senátní obvod 13 – Tábor" (.obvod == "1")
invitation = container.append \div
  ..attr \class \invitation
  ..html "Vyberte si volební obvod z mapky nebo z roletky vpravo nahoře, poté se zde zobrazí seznam všech kandidujících senátorů."
if window.location.hash
  id = that.slice 1
  if 0 <= id.indexOf "-"
    if senatAssoc[id]
      vizitky.display do
        \senat
        "Kandidát do senátu #{that.cislo}. #{that.jmeno} #{that.prijmeni}, obvod #{that.obvod_nazev}"
        -> it is that
      container.classed \is-share 1
  else
    obvodId = parseInt id, 10
    console.log obvodId
    return unless obvodId
    return unless obvodyAssoc[obvodId]
    mapka.onObvodSelected obvodyAssoc[obvodId]
